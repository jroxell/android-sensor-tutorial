package com.example.sensortutorial;

import java.util.List;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;
import android.hardware.Sensor;
import android.hardware.SensorManager;

public class SensorList extends ActionBarActivity {
	
	private TextView mSensorHeader;
	private TextView mSensorData;
	private SensorManager mSensorManager;

	public SensorList() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.sensor); // set view to sensor.xml
		mSensorHeader = (TextView)findViewById(R.id.sensor_header_text);
		mSensorHeader.setText("Sensor List");
		
		mSensorData = (TextView)findViewById(R.id.sensor_data);
		
		// create sensor manager 
		mSensorManager = (SensorManager)this.getSystemService(SENSOR_SERVICE);
		List<Sensor> list = mSensorManager.getSensorList(Sensor.TYPE_ALL);
		
		StringBuilder sensorStringBuilder = new StringBuilder();
		for (Sensor sensor : list) {
			sensorStringBuilder.append(sensor.getName() + "\n");
		}
		
		mSensorData.setText(sensorStringBuilder);
		
	}
}
