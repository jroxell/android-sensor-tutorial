package com.example.sensortutorial;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

public class AccelerometerSensor extends ActionBarActivity implements SensorEventListener {
	
	private TextView mSensorHeader;
	private TextView mSensorData;
	private Sensor mSensor;
	private SensorManager mSensorManager;
	private long lastUpdate=0;
	private float mLast_x;
	private float mLast_y;
	private float mLast_z;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.sensor);
		
		mSensorHeader = (TextView)findViewById(R.id.sensor_header_text);
		mSensorData = (TextView)findViewById(R.id.sensor_data);
		
		
		
		mSensorManager = (SensorManager)this.getSystemService(SENSOR_SERVICE);
		mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSensorManager.registerListener(this, mSensor,SensorManager.SENSOR_DELAY_NORMAL);
		
		if (mSensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			mSensorHeader.setText("Accelerometer gravidty : ca 9.8");
		}
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		
		float[] sensorValues = event.values;
		StringBuilder sensInfoBuilder = new StringBuilder();
		
		float x = event.values[0];
		float y = event.values[1];
		float z = event.values[2];
		
		long currentTime = System.currentTimeMillis();
		
		if ((currentTime - lastUpdate)>100) {
			
			long diffTime = (currentTime - lastUpdate);
			
			sensInfoBuilder.append("x: "+x+"\n");
			sensInfoBuilder.append("y: "+y+"\n");
			sensInfoBuilder.append("z: "+z+"\n");
			
			//float speed = Math.abs(x + y + z - mLast_x - mLast_y - mLast_z)/ diffTime * 10000;
			// test, tolka axlarna hur du vill 
			if (y>1) {
				sensInfoBuilder.append("moving backwards");
			} else if (y<-1) {
				sensInfoBuilder.append("moving forward");
			} else if (x>1) {
				sensInfoBuilder.append("moving left");
			} else if (x<-1) {
				sensInfoBuilder.append("Moving right");
			}
		
			lastUpdate = currentTime;
			//sensInfoBuilder.append("spped: "+speed);
			
			mSensorData.setText(sensInfoBuilder);
		}	
		//sensInfoBuilder.append(event.getClass().getName() + sensorValues);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		// register listener this class as a listener for the light sensor
		mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		// unregister listener on pause
		mSensorManager.unregisterListener(this);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
}
