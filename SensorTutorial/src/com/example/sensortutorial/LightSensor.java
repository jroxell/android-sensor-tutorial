package com.example.sensortutorial;

import java.security.PublicKey;
import java.util.List;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.R.integer;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class LightSensor extends ActionBarActivity implements SensorEventListener{
	
	private TextView mSensorHeader;
	private TextView mSensorData;
	private SensorManager mSensorManager;
	private Sensor mSensor;
	private LinearLayout mLayout;
	
	public LightSensor() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.sensor); // set view to sensor.xml
		
		mLayout = (LinearLayout)findViewById(R.id.main_layout);
		
		mSensorHeader = (TextView)findViewById(R.id.sensor_header_text);
		mSensorHeader.setText("Light sensor");
		
		mSensorData = (TextView)findViewById(R.id.sensor_data);
		
		// create sensor manager 
		mSensorManager = (SensorManager)this.getSystemService(SENSOR_SERVICE);
		
		// create light sensor
		mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		
		// register listener
		mSensorManager.registerListener(this,mSensor,SensorManager.SENSOR_DELAY_NORMAL);
			
		
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
	
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
	
		float[] sensorValues = event.values;
		StringBuilder sensInfoBuilder = new StringBuilder();
		
		int counter = 0;
		for (float values : sensorValues) {
			sensInfoBuilder.append(counter+" : "+values + "\n");
			counter++;
		}		
		//sensInfoBuilder.append(event.getClass().getName() + sensorValues);
		
		mSensorData.setText(sensInfoBuilder);
		
		if (sensorValues[0] < 20) {
			
			mSensorHeader.setText("I'ts kinda dark !");
			mSensorHeader.setTextColor(Color.WHITE);
			mLayout.setBackgroundColor(Color.BLACK);
		}else {
			mSensorHeader.setText("I can see!");
			mSensorHeader.setTextColor(Color.BLACK);
			mLayout.setBackgroundColor(Color.WHITE);
		}
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		// register listener this class as a listener for the light sensor
		mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		// unregister listener on pause
		mSensorManager.unregisterListener(this);
	}

}
