package com.example.sensortutorial;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class SensorTurorial extends ActionBarActivity {
	
	// 0. add some cool static variables for management
	public static final String TAG ="SensorTurorial"; // used for logging information
	
	// 1. create holders for each button
	private Button mLightSensorBtn;
	private Button mAccelerometerBtn;
	private Button mProximityBtn;
	private Button mOrientationBtn;
	private Button mMagneticBtn;
	private Button mSensorList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sensor_turorial);
		
		// 2. "hook" all components from the layout xml to the private member buttons
		mLightSensorBtn = (Button)findViewById(R.id.light_sensor_btn);
		mAccelerometerBtn = (Button)findViewById(R.id.accelerometer_sensor_btn);
		mProximityBtn = (Button)findViewById(R.id.proximity_sensor_btn);
		mOrientationBtn = (Button)findViewById(R.id.orientation_sensor_btn);
		mMagneticBtn = (Button)findViewById(R.id.magnetic_sensor_btn);
		mSensorList = (Button)findViewById(R.id.sensor_list_btn);
		
		// 3. add Action listener for each button
		
		
		mSensorList.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d(TAG, "mSensorList clicked.");
				// start activity
				Intent intent = new Intent(SensorTurorial.this,SensorList.class);
				startActivityForResult(intent, 0); // intent, request code
				
			}
		});
		
		
		mLightSensorBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d(TAG, "mLightSensorBtn clicked.");
				// start activity
				Intent intent = new Intent(SensorTurorial.this,LightSensor.class);
				startActivityForResult(intent, 1); // intent, request code
			}
		});
		
		mAccelerometerBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SensorTurorial.this, AccelerometerSensor.class);
				startActivityForResult(intent, 2);
			}
		});
		
		
		
	}
	
	
	
	
	/************** Don't give a flying fuck about what's beneath this line ***************/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sensor_turorial, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
